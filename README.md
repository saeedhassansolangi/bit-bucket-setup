#                 BitBucket
  
###       Pushing local repo to Bit Bucket
* first create a new repository on Bit Bucket
* then clone it into  computer 
* then goto that clone repo (cd repo)
* create new files/folders and do changes to it
* git add .
* git commit -m "message goes here"
* git push origin master ( and we are done )

###             Creating a  New Branch 
* git branh newBranch
* git checkout newBranch
* do changes and play with project 
* git add .
* git commit -m "message"
* git push --set-upstream origin newBranch

###         Merging the new Brances to master
* git checkout master
* git merge newBranch
* git add .
* git commit -m "message"
* git push origin master